"use strict";
/*Дан массив books.
Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.
*/

//
const root = document.querySelector("#root");
const list = document.createElement("ul");
root.append(list);

const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

books.forEach((item) => {
  const items = document.createElement("li");
  list.append(items);
  const { author, name, price } = item;
  try {
    if (author && name && price) {
      items.append(
        `Автор книги :  ${author}
          Название книги :   ${name}
            Цена за книгу :   ${price}`
      );
    } else {
      const {
        author = "Отсутствует имя Автора",
        name = "Отсутствует название книги ",
        price = "Мы пока еще не приудмали цену",
      } = item;
      items.style.display = "none";
      throw new Error(`${author} || ${price} || ${name}`);
    }
  } catch (e) {
    console.log(e);
  }
});
